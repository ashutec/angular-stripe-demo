import { StripePage } from './app.po';

describe('stripe App', () => {
  let page: StripePage;

  beforeEach(() => {
    page = new StripePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
