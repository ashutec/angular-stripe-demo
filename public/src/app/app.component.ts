import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StripeService, Elements, Element as StripeElement, ElementsOptions, SourceData } from "ngx-stripe";
import { WindowRef, STRIPE_PUBLISHABLE_KEY } from "ngx-stripe";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  elements: Elements;
  card: StripeElement;
  @ViewChild('card') cardRef: ElementRef;

  // optional parameters
  elementsOptions: ElementsOptions = {
    locale: 'es'
  };
  stripeTest: FormGroup;
  constructor(
    private http: HttpClient,
    private afs: AngularFirestore,
    private fb: FormBuilder,
    private stripeService: StripeService,
    private window: WindowRef) {

  }
  stripCollection: AngularFirestoreCollection<any> = this.afs.collection('stripe');
  ngOnInit() {
    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            style: {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                  color: '#CFD7E0'
                }
              }
            }
          });
          this.card.mount(this.cardRef.nativeElement);
        }
      });
  }

  buy() {
    const name = this.stripeTest.get('name').value;
    this.stripeService
      .createToken(this.card, { name })
      .subscribe(token => {
        if (token) {
          const amount = 50;
          const type = "card";
          const currency = "USD";
          const tokenId = token.token.id;
          this.http.post("https://26b49010.ngrok.io/api/stripeCharge/charge", token)
            .subscribe((res: any) => {
              let id = this.afs.createId();
              this.stripCollection.doc(id).set({
                card: res.id,
                currancy: res.currency,
                description:res.description
              })
                .catch((err) => {
                console.log(err);
              })
            },
            err => {
              console.log("Error occuredss");
            }
            );
          // https://stripe.com/docs/charges
          console.log(token.token.id);
        } else if (token) {
          // Error creating the token
          console.log(token);
        }
      });
  }
}
