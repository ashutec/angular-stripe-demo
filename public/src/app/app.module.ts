import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NgxStripeModule } from 'ngx-stripe';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestore} from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import {stripeConfig} from '../../src/config';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
   
    NgxStripeModule.forRoot(stripeConfig.publishableKey),
  ],
  providers: [ AngularFirestore,],
  bootstrap: [AppComponent]
})
export class AppModule { }
