const stripeChargeController = require('./stripeCharge.controller');
const router = require('express').Router();



router.post('/charge', stripeChargeController.charge);

module.exports = router;
