const config = require("../config");
const keyPublishable = config.publishableKey;
const keySecret = config.keySecret;
const stripe = require("stripe")(keySecret);
const controller = {

    async charge(req, res) {
        var token = req.body.token.id;
        const customer = await stripe.customers.create({
            email: "ashutec.demo@gmail.com",
            source: token
        });
        stripe.charges.create({
            amount: 60,
            description: "Sample Charge",
            currency: "usd",
            customer: customer.id
        })
            .then(charge => {
                res.send(charge);
                //  res.status(200).res.send(charge)
            })
            .catch(err => {
                console.log("Error:", err);
                res.status(500).send({ error: "Purchase Failed" });
            });
    },
};
module.exports = controller;
