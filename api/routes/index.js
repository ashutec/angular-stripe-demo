const router = require('express').Router();
const {stripeChargeRoutes} = require('../stripeCharge');
const initialize = (app) => {
    app.get('/', (req, res) => {
        res.send('App works!');
    });
    app.use('/api', router);
    router.use('/stripeCharge', stripeChargeRoutes);
};  

module.exports = {
    initialize,
  };